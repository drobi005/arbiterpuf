library ieee;
use ieee.std_logic_1164.all;

entity ArbiterPUF is 
	generic (BitWidth : integer := 4); --N-Bit generic
	port(
		CLK        : in  std_logic;
		RST        : in  std_logic;
		Input	     : in  std_logic;  --initial input bit for MUX
		Challenge  : in  std_logic_vector(BitWidth-1 downto 0);  --N-bit Challenge 
		Key        : out std_logic);
end entity;

architecture Behaviorial of ArbiterPUF is
	component MUX21
		port(
			A   : in  std_logic;
			B   : in  std_logic;
			SEL : in  std_logic; --Current challenge bit
			Y   : out std_logic);
	end component;
	
	component DFlipFlop is
		port(
			D,CLK  : in std_logic;
			Q      : out std_logic);
	end component;
	
	signal Response   : std_logic;
	signal v,w      	: std_logic_vector(BitWidth-1 downto 0); --Mux output/input signals 
begin

	GEN:
	for i in 0 to (BitWidth-1) generate
		Initial_State: if (i = 0) generate
			M1  : MUX21 port map(A=>Input, B=>Input,SEL=>Challenge(i),Y=>v(i));
			M2  : MUX21 port map(A=>Input, B=>Input,SEL=>Challenge(i),Y=>w(i));
		end generate Initial_State;
		
		Main_State: if ((i > 0 AND i < (BitWidth-1))) generate
			M3  : MUX21 port map(A=>v(i-1), B=>w(i-1),SEL=>Challenge(i),Y=>v(i));
			M4  : MUX21 port map(A=>w(i-1), B=>v(i-1),SEL=>Challenge(i),Y=>w(i));
		end generate Main_State;
		
		Final_State: if (i = (BitWidth-1)) generate
			M5  : MUX21 port map(A=>v(i-1), B=>w(i-1),SEL=>Challenge(i),Y=>v(i));
			M6  : MUX21 port map(A=>w(i-1), B=>v(i-1),SEL=>Challenge(i),Y=>w(i));
			DFF : DFlipFlop port map (D=>v(i), CLK=>w(i), Q=>Response);
		end generate Final_State;
	end generate GEN;
	
	key <= Response;
--	process (CLK,finished,v,w) is
--		variable flag : integer :=0;
--		variable counter : integer :=0;
--	begin
--		if rising_edge(CLK) then
--			if (RST = '1') then
--				keyOut <= (others => '0');
--				finished <= 0;
--				N <= 0; 
--				i <= 0;
--				state <= s0;
--			elsif (finished = 1) then
--				keyOut(N) <= Response; --Store the response bit into respective keyOut position
--				N <= N +1; --Increment N by 1; to represent the next Input bit to get
--				finished <= 0;
--				i <= 0;    --reset finished, i and state back to zero
--				state <= next_state;
--			elsif (i = 0 AND finished = 0) then
--			elsif ((i > 0 AND i < BitWidth) AND finished = 0) then
--			elsif (i = (BitWidth-1) AND finished = 0) then
--				finished <= 1;
--			end if;
--		end if;
--	end process;
end architecture; 